# Management

We will use `manage.py` file to manage our project locally.

Lets setup a virtual environment and start working on our project:

> `virtualenv venv -p python3`:

It tells to the `virtualenv` function to create a virtual environment inside of a `venv` directory and use python 3 as a default python version in that virtual environment.

Activate your virtual environment using `source venv/bin/activate` command. To check if the environment is changed, execute `which python` command and check that your current python is inside of the `venv` folder.

Usually, `requirements.txt` file is used to write list of python libraries needed by the project to work. Every time when you want to install a library using pip, just add it to your `requirements.txt` file.
Currently, our project is only using `Django` library. Create the `requirements.txt` file and add `Django==2.1.2` line to this file.
To install libraries listed in the `requirements.txt` file, you will need to run `pip install -r requirements.txt` command.

You can also check list of installed libraries by running `pip freeze` command.


Now we have created a virtual environment and installed Django library in that environment. The next step will be to prepare the database and run our application:

* `./manage.py makemigrations` - It will prepare migrations for your applications. When you create a model for you application, it adds a migration to create a table for that model in your database. Also, when you change and delete fields from your models, this command will create a migration to make those changes in your database.
* `./manage.py migrate` - It will apply the migration files to your database.
* `./manage.py runserver` - It will run a test server at your local host (http://127.0.0.1:8000).

Run these commands and then open "http://127.0.0.1:8000" in your browser. If you have enabled the debug mode in the application settings, you will see a success page in that address.

Now, lets stop the server (CTRL+C) and create a super (admin) user: Run `./manage.py createsuperuser` command.

After creating a super user, run the server again (`./manage.py runserver`) and go to "http://127.0.0.1:8000/admin/" page and login using this new user credentials.


Play a bit with the admin page, check other commands in the `manage.py`.

# NOTES

* Running `./manage.py` will list available commands in your application. We will add some custom commands in our future lectures.
* Run `./manage.py command_name -h` to see a help text for that command.
