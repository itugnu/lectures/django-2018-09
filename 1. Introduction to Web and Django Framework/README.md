# What is The WEB

The Internet is a global system of interconnected computer networks that interchange data by packet switching using the standardized Internet Protocol Suite (TCP/IP). (Wikipedia) - The Internet is a network of networks, defined by the TPC/IP standards.

The World Wide Web (WWW, or simply Web) is an information space in which the items of interest, referred to as resources, are identified by global identifiers called Uniform Resource Identifiers (URI). (W3)

The Web is an information space. The first three specifications for Web technologies defined URLs, HTTP, and HTML.

The World Wide Web Consortium is the main international standards organization for the World Wide Web.

URI Schemas: mailto, ftp, news, tel, ldap, http and so on.

# Web Resources

The main web resource is HTML formatted *text documents*.
Nowadays web is much more broad than that: Web sites, Web APIs and so on.

# Backend and Frontend

API and Website related web resources are consisting of 2 main layers (parts): Backend and Frontend.

Designers design the UI (User Interface) part of a website, Frontend developers make them real using HTML, CSS and JavaScript.
Frontend has lots of frameworks (jQuery, Angularjs, Reactjs and so on) and new technologies (HTML5, CSS3).

Frontend is run on client (user) side (in browsers) and can be *manupulated by users*.

Backend Developers make a system that will handle the logic, analyze and etc. related to the *DATA*.
Frontend layer receives data from backend and makes verifications in backend.

If you want to develop a resource with a dynamic data, you will need a backend layer.

* Frontend - presentation layer, client-side layer
* Backend - data access layer, server-side layer

# Common Gateway Interface (CGI - Backend Layer)

Common Gateway Interface is a standard protocol to execute console applications (CLI) that produce web pages.
It has standard input (POST data, mostly from HTML Forms) and standard output.

You can develop such applications using Perl, PHP, Java, Python and other programming languages.

* A Web application needs *Request Methods*: GET, POST, PUT, PATCH, DELETE.
* It needs [status codes](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes) for the browsers to understand what is going on: 200, 204, 301, 400, 403, 404, 500 and etc.
* It needs some [headers](https://en.wikipedia.org/wiki/List_of_HTTP_header_fields) for browsers to understand how to parse the document, the response: Content-Type, Cache-Control, Content-Encoding, Authorization, WWW-Authenticate, Content-Length, Set-Cookie, Date and etc.
* It might need to handle some unsafe methods (POST, PUT, PATCH, DELETE) using some methods like X-CSRF.
* It needs to understand URI schemas and paths.
* It might need to be able to work with different content-types: HTML, Json, XML and etc.

# Web Frameworks

There are some web frameworks like Symfony, Laravel, Drupal, Ruby on Rails, ASP.NET, Spring, Flask, Django that helps you to implement these functionalities easier and faster.
Web Framework is a collection of tools that implements functionalities needed to build a web application.

# Our Focus for the Lecture

Our main focus in this course is building a backend application using Django Web Framework.
We will be making a base structure for the Web Application, have some **Views** to show the data on frontend layer, have an API for usage in different applications (JavaScript frontend frameworks, Android & iOS Application, Web Services).

# NEXT

Read BASIC_SETUP.md file for the outcomes of this lecture.
