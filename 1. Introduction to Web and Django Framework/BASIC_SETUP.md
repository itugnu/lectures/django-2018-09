# Start Project

From the previous lecture we already have `virtualenv` and `django-admin` commands.

Start a new django project using `django-admin startproject musicnet`.

That will create a `musicnet` folder. For this lecture we will develop an IMDB-like website for music.

Lets go to the folder and see what we have: `cd musicnet`.

You will see some folders and files:

* `manage.py` - a main command line tool (CLI) that we will use to control our app.
* `musicnet` - a folder with the main configurations for our app.
* `musicnet/__init__py` - an empty file used to identify a folder as an application.
* `musicnet/settings.py` - a file where we will keep our app settings.
* `musicnet/urls.py` - URL paths for the website, for example: `/about/`, `/blog/`, `/albums/`.
* `musicnet/wsgi.py` - a [WSGI-compatible](https://en.wikipedia.org/wiki/Web_Server_Gateway_Interface) interface.

Lets go over available configurations (settings):

* `BASE_DIR` - the directory where our app is located. It is used for building a relative path for other settings like Database file, static (css, js, images, audio, video) and media (uploaded by user) files.
* `SECRET_KEY` - a key that is needed for cookie encryption, it should be kept secret and not shared with anyone.
* `DEBUG` - used to activate or diactivate the debug mode.
* `ALLOWED_HOSTS` - list of [Hostnames](https://en.wikipedia.org/wiki/Hostname) that can access to the website. You will need to list all domains and subdomains in order to be able to view the website *in that domain*. You can also use `'*'` value to allow all.
* `INSTALLED_APPS` - list of applications installed in this project. It can be your own applications or applications installed from other sources.
* `MIDDLEWARE` - list of functions that will be executed between users' request and your views. It can be used to detect user's language preferences and change the website locale (language), make some checks before a user access any page in the system and etc.
* `ROOT_URLCONF` - the main `urls.py` file that will be a start point for the project. Usually it is in the main configuration folder (`musicnet`).
* `TEMPLATES` - Configuration for the template engine and the folders containing template files. More about that in the [TEMPLATES](#templates) section.
* `WSGI_APPLICATION` - the wsgi.py file.
* `DATABASES` - Database configuration. More about that in the [DATABASES](#databases) section.
* `AUTH_PASSWORD_VALIDATORS` - list of password validation functions. By default there are functions that will prevent similarities between user's name and password, minimum length of the password, some common passwords and etc. You can easily add your own validating functions.
* `LANGUAGE_CODE` - the main language in the system. The default value is `en-us`, you can change it to `tr` for Turkish. You can have multiple languages in your application. We will talk about this in future lectures.
* `TIME_ZONE` - Django is storing time and date related data in your database with a time zone value. It is mostly used to handle time differences between users and the server. If you app is international, you might prefer to use `UTC` time zone, but if it is a regional app (like ITUGnu, it is for Turkey only) use according time zone (`Europe/Istanbul` for Turkey).
* `USE_TZ` - if you need django to handle time zone fields with the chosen time zone in your `TIME_ZONE` setting, you will need to set it to True.
* `USE_I18N` - enable this setting to use internationalization (i18n) in your app. This will allow to use regional formats (ex: number format, date formats) in your app (forms, views and etc.).
* `USE_L10N` - enable this setting to use localization (l10n) in your app. This will allow to use multiple languages in your app. We will discuss adding other languages in future lectures.
* `STATIC_URL` - addition to the URL for static files. Having `/static/` means your static files will be available at "http://yourdomain.com/static/".

We will add a few more settings to settings in future lectures.

# TEMPLATES

* `BACKEND` - which template engine to use. You need a template engine for rendering html and other templates. Django by default is using Django Template Engine (`django.template.backends.django.DjangoTemplates`). You can also use Jinja2 engine (`django.template.backends.jinja2.Jinja2`) or find template engines for other formats.
* `DIRS` - a list of directories where the engine should look for template source files, in search order. We leave it empty as we a not going to use custom templates folders.
* `APP_DIRS` - whether the engine should look for templates inside installed applications. For Django Template Engine it is `templates` folder inside applications. We enabled it as each our app will have it's own templates.
* `OPTIONS` - list of backend-specific settings.
* `context_processors` - an option for Django Template Engine. While rendering your templates you will pass some values for using in each template. There might be some data that needs to be used in all templates (like user details) and they can be provided directly from the context processors to *all* templates.

# DATABASES

* `default` - the main database that will be used for the app. You can add multiple databases for some other use cases. The default database will be used to manage your app's main data.
* `ENGINE` - a database engine that will be required. Django ORM (Object-relational mapping) supports SQLite, PostgreSQL, MySQL, Oracle and other databases. For local development we will be using SQLite3 as it is a built-in engine in Python language and uses a local file to store data. Later we will learn how to set-up it for the PostgreSQL database in our DEPLOYMENT topic.
* `NAME` - the name of the database to use. In SQLite it will be the path of the file. By using `os.path.join(BASE_DIR, 'db.sqlite3')` value we are building a relative path to have a `db.sqlite3` file inside our project folder.
* `HOST` - the database host as an address (`127.0.0.1`) or a unix socket (`/var/run/mysql`).
* `PORT` - port for the database host address.
* `USER` - user for the database.
* `PASSWORD` - password for our database.

# Django Packages

Django apps can be found at [Django Packages](https://djangopackages.org/). They are just like usual python libraries and can be downloaded from [Pypi](https://pypi.org/) using the `pip` CLI tool.

# URLS

In django, we don't serve python files directly like other CGI apps (PHP, Some simple python web applications and etc.). Usually we only serve the `wsgi.py` file and all pages are coming from the `urls.py` file that forwards the request to the according functions or view objects.

Starting from Django 2.0 we use `path` function to make a list of pages. `musicnet/urls.py` file contains the default `urlpatterns` list for our app. We can have multiple `urls.py` files in our project, but they must be included in that default file to work.

Lets go over available parameters for the `path` function:

* Address - the first parameter, it will be the path for the view. For example, `about/` will create a page `http://yourdomain.com/about/`.
* View function or view object - the second parameter, where the request will be passed.
* Name - an optional parameter to give that URL a name that can be used in your templates or other app code. We will use it a lot to avoid giving a direct URL in our templates and redirects.

In our default `urls.py` file we will have a line `path('admin/', admin.site.urls)` which will add an admin page in our website: `http://yourdomain.com/admin/`. That parameter will pass requests coming to the `/admin/` page to django admin app. It also defines other addresses inside that app. They will be merged to the `/admin/` path (like `/admin/auth/user/`).

# NEXT

Read `MANAGEMENT.md` file for management commands.
