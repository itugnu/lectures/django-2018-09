# Setup

For this course, we will use Django 2.1 and python 3.7

Use `python3 -V` to check your python version. Your version should be 3.5+ (3.5, 3.6, 3.7).

If you have an older version, install the newer one:

Ubuntu, Mint, Debian Python Install
-----------------------------------

* Update your package index: `sudo apt-get update`
* Search for python3 package: `sudo apt-cache search python3`
* If there is no the newer version: `sudo add-apt-repository ppa:deadsnakes/ppa`
* Update index again if you added the PPA: `sudo apt-get update`
* Install the package: `sudo apt-get install python3.7`
* Install the `pip` package: `sudo apt-get install python3-pip`
* Install `virtualenv` tool: `sudo pip3 install -U virtualenv`

Arch Linux Python Install
-------------------------

Arch Linux (and Manjaro) usually comes with python 3 by default. Check your version.

* Update your packages: `sudo pacman -Syu`
* If you don't have python 3, install: `sudo pacman -S python`
* Install the `pip` package: `sudo pacman -S python-pip`
* Use `pip -V` to verify python 3 is your default. If not, check with `pip3 -V` and use `pip3` for other packages.
* Install `virtualenv` tool: `sudo pip install -U virtualenv`

Install Django
--------------

* Install Django as a system package, we will need django-admin command line tool: `sudo pip install Django==2.1.2`

# Editors

Students are free to use any Editors they want.
I personally suggest using [Atom Editor](https://atom.io/) as a light code editing tool, [PyCharm Community Edition](https://www.jetbrains.com/pycharm/download) as IDE.

# Notes:

* Django-admin command will be needed to create a base structure for a Django project.
* Virtualenv tool is for project isolation. It will isolate your project python files from the system libraries. That makes a safe environment to work with.
* Using automated tools (like IDE tools) for the app is not allowed in this course, students need to learn working with django management commands. It will be needed for the deployment and testing topic.
