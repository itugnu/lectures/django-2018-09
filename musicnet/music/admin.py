from django.contrib import admin
from music.models import Artist, Album, Music


@admin.register(Artist)
class ArtistAdmin(admin.ModelAdmin):
    fields = ('name', 'birthday')
    list_display = ('name', 'birthday', 'musics')
    search_fields = ('name',)

    def musics(self, obj):
        music_count = Music.objects.filter(singer=obj).count()
        return f"{music_count}"

    musics.short_description = "Musics"


@admin.register(Album)
class AlbumAdmin(admin.ModelAdmin):
    fields = ('title', 'composer', 'release_date')
    list_display = ('title', 'composer', 'release_date')
    list_filter = ('composer', 'release_date')
    search_fields = ('title',)
    autocomplete_fields = ('composer',)


@admin.register(Music)
class MusicAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('title', 'album', ('singer', 'composer'), 'release_date')
        }),
        ('Record Details', {
            'classes': ('collapse',),
            'fields': ('updated_at', 'created_at')
        })
    )
    list_display = ('title', 'singer', 'album', 'release_date')
    list_filter = ('singer', 'release_date',)
    search_fields = ('title',)
    readonly_fields = ('updated_at', 'created_at')

# admin.site.register(Artist, ArtistAdmin)
