from django.db import models


class Artist(models.Model):
    name = models.CharField("Name Surname", max_length=150, blank=False, null=False)
    birthday = models.DateField("Birthday")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Artist"
        verbose_name_plural = "Artists"
        ordering = ('name',)


class Album(models.Model):
    title = models.CharField("Title", max_length=255)
    composer = models.ForeignKey(Artist, verbose_name="Composer", on_delete=models.CASCADE, related_name='albums')
    release_date = models.DateField("Release Date", blank=True, null=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Album"
        verbose_name_plural = "Albums"
        ordering = ('title', '-release_date',)


class Music(models.Model):
    title = models.CharField("Title", max_length=255)
    album = models.ForeignKey(
        Album, verbose_name="Album", on_delete=models.SET_NULL, related_name='musics',
        blank=True, null=True
    )
    singer = models.ForeignKey(Artist, verbose_name="Singer", on_delete=models.CASCADE, related_name='musics')
    composer = models.ForeignKey(
        Artist, verbose_name="Composer", on_delete=models.SET_NULL, related_name='composed_musics',
        blank=True, null=True
    )
    release_date = models.DateField("Release Date", blank=True, null=True)
    updated_at = models.DateTimeField("Updated", auto_now=True, blank=True, null=True)
    created_at = models.DateTimeField("Created", auto_now_add=True)

    def __str__(self):
        title = self.title
        singer = self.singer  # use __str__ function of the Artist object
        return f"{title} by {singer}"  # formatted string

    class Meta:
        verbose_name = "Music"
        verbose_name_plural = "Musics"
        ordering = ('title', 'release_date',)
