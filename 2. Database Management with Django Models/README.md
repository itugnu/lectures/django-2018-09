# Database Management (DB Models)

Django has a built-in ORM (Object Relation Mapping) support which makes it a lot easier to work with data.

First of all, we need to write our data model (representation) in our `models.py` file.
Then django will generate a migration (SQL) queries to create a database for that model.
We can use our new model in our application just like a native python classes and django will manage the part where you need to store, update or delete your data in the database.

As we discussed before, django has built-in support for MySQL, Oracle, SQlite3 and PostgreSQL database engines.
If your are not familiar with database engines, these are programs that helps you to efficiently store and manage your data (sort, filter, order, mix and etc.).

Django's ORM is built especially for the PostgreSQL database engine and there are some functionalities that are only supported for the PostgreSQL engine. In our previous course we learned how to choose and configure the database engine in django settings. For simplicity reasons, we will continue to use SQlite3 database engine in the course.

First of all, lets create an app to store our music data:

`$ ./manage.py startapp music`

This command will create a `music` folder with some files in it:

* `migrations/` - a folder where our database migrations (creation and updates for our model) will be stored.
* `__init__.py` - an empty file used to identify a folder as an application.
* `admin.py` - a file where we will register our models for the django admin tool.
* `apps.py` - definition for our application. We will use it later in the course.
* `models.py` - a file where we will write our models.
* `tests.py` - a file where we will write tests for our model and its views.
* `views.py` - a file where views for that model will be written.

For this part of the lecture, we will focus on `models.py` file.

It comes with one import line: `from django.db import models`

In order to create a new model, we will need to extend Django's `models.Model` class and it will automatically provide us tools for managing our data.

Your data may contain data with different fields (text, integer, boolean, decimal, date, time and etc.). Check the [list of supported model field types](https://docs.djangoproject.com/en/2.1/ref/models/fields/#field-types) for more and I would recommend to use this page every time when you are writing a new model. It has some special fields like `EmailField` for storing email addresses.

Each field has special attributes, essentials:

* `null` - describes if the field can be None (NULL).
* `blank` - if the field can be empty. If you are using null, then it is suggested to also enable blank.
* `verbose_name` - the name of the field. For most of them it is the first argument and can be directly written without providing a parameter name (see `CharField` in our examples).

Also, for `CharField` you have to provide `max_length` parameter which is the limit of the characters for that field. I wouldn't suggest using more than 255 characters as a limit due to the database architecture, if you need longer fields, check `TextField`.

You can also make a relation between different models. Relation is when you connect 2 different models to each other. You can [read more about database relations](https://en.wikipedia.org/wiki/One-to-many_%28data_model%29).

To make a Many-to-one relation, where a mother can have multiple children, we use `ForeignKey`. First argument of the `ForeignKey` should be the target object (for a child, target object is mother). These attributes are mostly used for a `ForeignKey` field:
* `to` - the first argument, a target object where the model will connect.
* `verbose_name` - the name of the field.
* `on_delete` - the behaviour when a target object is deleted. It has few options:
  - `CASCADE` - delete the child object if the target object is deleted.
  - `PROTECT` - will not allow to delete a target model if it has any children.
  - `SET_NULL` - sets the target to NULL, you have to make the field nullable (`null=True`) for this option.
  - `SET_DEFAULT` - set to the default value.
  - `SET` - similar to the `SET_DEFAULT`, but you can provide a custom function were you will be able to dynamically choose the default value.
  - `DO_NOTHING` - do nothing. It will give an `IntegrityError` and crash your app when you will try to access an object with the Foreign Key that was deleted.
* `related_name` - a name that will be used to get the related child objects from the target: `mother.children`. You can set it to "+" to disable the backward relation.

Another useful field is `DateTimeField` that stores date and time. It has 2 special parameters:
* `auto_now` - automatically set the date and time to the current time when the model is changed (saved).
* `auto_now_add` - automatically set the date and time to the current time when **creating the model for the first time**. It will not update the field later.

Each model should have a `Meta` class that will describe it to the application. In this mode:
* `verbose_name` - Model's name, i.e. album.
* `verbose_name_plural` - Model's name in the plural form, i.e. albums.
* `ordering` - a tuple that will tell how your data will be sorted (ordered).
* You can also manage table name for the database and etc. Check possible options [here](https://docs.djangoproject.com/en/2.1/ref/models/options/).

Also, it is better to add an `__str__` function for models to have a logical name for each object. By default, it will be `<Model_Name object id_number>` which usually do not mean anything. This method should return a string (`str` type).

That should be enough to have a useful model for your application. Now you should register your application in the project to activate it.
Open `musicnet/settings.py` file and in the end of the `INSTALLED_APPS` list, add your new app: `music`.

Create database migrations: `./manage.py makemigrations`

This command will create a migration files inside the `music/migrations/` folder. You can check them if you would like to know how django manages table creations.

Now you will need to apply this migration to your database: `./manage.py migrate`


# NEXT

Read `ADMIN.md` file for django admin tool where you can easily test and manage your models.
